import { Component, OnInit } from '@angular/core';
import { default as data } from '../assets/templates';
import { THROW_IF_NOT_FOUND } from '../../node_modules/@angular/core/src/di/injector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  selectedIndex = 0;
  selected = data[this.selectedIndex];
  firstThumb = 0;
  thumbs = data.slice(0,4);
  prevDisabled = true;
  nextDisabled = false;

  public onSelect(id) {
    this.selectedIndex = data.findIndex(x => x.id == id);
    this.selected = data[this.selectedIndex];
  }

  public onNext() {
    this.prevDisabled = false;
    if(this.firstThumb < 8) {
      this.firstThumb += 4;
      this.thumbs = data.slice(this.firstThumb, this.firstThumb + 4);
    } else {
      this.nextDisabled = true;
      this.firstThumb = 12;
      this.thumbs = data.slice(12, 15);
    }
  }

  public onPrevious() {
    this.nextDisabled = false;
    if(this.firstThumb > 4) {
      this.firstThumb -= 4;
      this.thumbs = data.slice(this.firstThumb, this.firstThumb + 4);
    } else {
      this.prevDisabled = true;
      this.firstThumb = 0;
      this.thumbs = data.slice(0, 4);
    }
  }
} 