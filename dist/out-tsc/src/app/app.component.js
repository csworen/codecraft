"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var templates_1 = require("../assets/templates");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.selectedIndex = 0;
        this.selected = templates_1.default[this.selectedIndex];
        this.firstThumb = 0;
        this.thumbs = templates_1.default.slice(0, 4);
        this.prevDisabled = true;
        this.nextDisabled = false;
    }
    AppComponent.prototype.onSelect = function (id) {
        this.selectedIndex = templates_1.default.findIndex(function (x) { return x.id == id; });
        this.selected = templates_1.default[this.selectedIndex];
    };
    AppComponent.prototype.onScroll = function (i) {
        if (i == 1) {
            if (this.firstThumb >= 8) {
                this.nextDisabled = true;
                this.firstThumb = 12;
                this.thumbs = templates_1.default.slice(12);
            }
            else if (this.firstThumb == 0) {
                this.prevDisabled == false;
                this.firstThumb += 4;
                this.thumbs = templates_1.default.slice(4, 8);
            }
            else {
                this.firstThumb += 4;
                this.thumbs = templates_1.default.slice(this.firstThumb, this.firstThumb + 4);
            }
        }
        else {
            if (this.firstThumb <= 4) {
                this.prevDisabled = true;
                this.firstThumb = 0;
                this.thumbs = templates_1.default.slice(0, 4);
            }
            else if (this.firstThumb == 12) {
                this.nextDisabled == false;
                this.firstThumb = 8;
                this.thumbs = templates_1.default.slice(8, 12);
            }
            else {
                this.firstThumb -= 4;
                this.thumbs = templates_1.default.slice(this.firstThumb, this.firstThumb + 4);
            }
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map