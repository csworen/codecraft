# Codecraft

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Code scaffolding

To install dependencies, use `npm install` inside the directory.
As some issues will crop up (experimental/new version of Angular), you will also need to use `npm audit fix --force` to automatically fix the 1 critical issue.

## Development server

Run `ng serve --open` for a dev server. The `--open` flag will automatically navigate to `http://localhost:4200/` to display the app.
